#pragma once
#define _USE_MATH_DEFINES
#include <cmath>
#undef _USE_MATH_DEFINES

#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <experimental/filesystem>
#include <complex>

namespace mycalculator
{
	class Calculator
	{
	private:
		enum class TokenValue
		{
			NAME, NUMBER,
			PLUS = '+', MINUS = '-', MUL = '*', DIV = '/',
			PRINT = ';', ASSIGN = '=', LP = '(', RP = ')'
		};

		std::string expr_;
		std::string::iterator itr_;
		TokenValue curr_tok_;
		std::complex<double> number_variable_;
		std::string string_variable_;
		std::map<std::string, std::complex<double>> table_;
		std::ifstream fin_;
		bool isfile_;

		TokenValue GetToken();
		std::complex<double> Prim(bool);
		std::complex<double> Term(bool);
		std::complex<double> Expr(bool);

	public:
		Calculator() {};
		~Calculator() {};

		void Calculate(const std::string& expr);
	};
	int Error(const std::string &s);
	bool isdotordig(char simbol);
}