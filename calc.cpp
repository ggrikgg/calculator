#include "stdafx.h"

#include "calc.h"




bool mycalculator::isdotordig(char simbol)
{
	return isdigit(simbol) || (simbol == '.');
}
int mycalculator::Error(const std::string &s)
{
	std::cerr << "Error: " << s << '\n';
	return 1;
}
mycalculator::Calculator::TokenValue mycalculator::Calculator::GetToken()
{
	while (itr_ != expr_.end())
	{
		while (*itr_ == ' ')
			itr_++;
		switch (*itr_)
		{
		case ';':
		case '\n':
			return curr_tok_ = TokenValue::PRINT;
		case '*':
		case '/':
		case '+':
		case '-':
		case '(':
		case ')':
		case '=':
			curr_tok_ = TokenValue(*itr_++);
			return curr_tok_;
		case '0': case '1': case '2': case '3': case '4':
		case '5': case '6': case '7': case '8': case '9':
		case '.':
		{
			auto it = find_if(itr_, expr_.end(), isdotordig);
			if (it != expr_.end())
			{
				double temp = std::atof((expr_.data() + (it - expr_.begin())));
				if ((it + 1) != expr_.end())
					if (*(it + 1) == 'i')
					{
						number_variable_ = std::move(std::complex< double >(0, temp));
						it++;
					}
					else
					{
						number_variable_ = std::move(temp);
					}
				else
				{
					number_variable_ = std::move(temp);
				}
			}
			while (itr_ != expr_.end() && (isalnum(*itr_) || (*itr_ == ',')))
				itr_++;
			return curr_tok_ = TokenValue::NUMBER;
		}
		default:
			if (isalpha(*itr_))
			{
				string_variable_ = *itr_++;
				while (itr_ != expr_.end() && isalnum(*itr_))
				{
					string_variable_.push_back(*itr_);
					itr_++;
				}
				return curr_tok_ = TokenValue::NAME;
			}
			Error("Error lexem");
			return curr_tok_ = TokenValue::PRINT;
		}
	}
}
std::complex<double> mycalculator::Calculator::Prim(bool get)
{
	if (get)
		GetToken();
	switch (curr_tok_)
	{
	case TokenValue::NUMBER:
	{
		std::complex<double> v = number_variable_;
		GetToken();
		return v;
	}
	case TokenValue::NAME:
	{
		std::complex<double>& v = table_[string_variable_];
		GetToken();
		if (curr_tok_ == TokenValue::ASSIGN)
			v = Expr(true);
		return v;
	}
	case TokenValue::MINUS:
		return -Prim(true);
	case TokenValue::LP:
	{
		std::complex<double> e = Expr(true);
		try
		{
			if (curr_tok_ != TokenValue::RP)
				throw 3;
			GetToken();
			return e;
		}
		catch (int i)
		{
			return Error("��������� )");
		}
	}
	default:
		return Error("��������� ��������� ���������");
	}
}
std::complex<double> mycalculator::Calculator::Term(bool get)
{
	std::complex<double> left = Prim(get);
	for (;;)
	{
		switch (curr_tok_)
		{
		case TokenValue::MUL:
			left = left* Prim(true);
			break;
		case TokenValue::DIV:
			try
			{
				std::complex<double> d = Prim(true);
				if (d == std::complex<double>(0, 0))
				{
					throw 2;
				}
				left /= d;
				break;
			}
			catch (int i)
			{
				return Error("������� �� 0");
			}
		default:
			return left;
		}
	}
}
std::complex<double> mycalculator::Calculator::Expr(bool get)
{
	std::complex<double> left = Term(get);
	for (;;)
	{
		switch (curr_tok_)
		{
		case TokenValue::PLUS:
			left = left + Term(true);
			break;
		case TokenValue::MINUS:
			left = left - Term(true);
			break;
		default:
			return left;
		}
	}
}
void mycalculator::Calculator::Calculate(const std::string& expr)
{
	table_.clear();
	table_["pi"] = M_PI;
	table_["e"] = M_E;
	table_["i"] = std::complex< double >(0, 1);
	int mystringlen = expr.length();
	if (mystringlen > 5 && expr[mystringlen - 5] == '.' && expr[mystringlen - 4] == 'c'
		&& expr[mystringlen - 3] == 'a'&& expr[mystringlen - 2] == 'l'&& expr[mystringlen - 1] == 'c')
	{
		isfile_ = true;
		fin_.open(expr);
		if (!fin_.is_open())
		{
			Error("File cant be open!\n");
			return;
		}
		try
		{
			while (fin_ >> expr_)
			{
				itr_ = expr_.begin();
				GetToken();
				std::cout << Expr(false) << std::endl;
			}
		}
		catch (std::exception& e)
		{
			std::cerr << "exception caught: " << e.what() << '\n';
		}
		fin_.close();
	}
	else
	{
		isfile_ = false;
		expr_ = expr;
		itr_ = expr_.begin();
		GetToken();
		std::cout << Expr(false) << std::endl;
	}
}